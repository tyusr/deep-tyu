# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com

import pandas as pd
from PIL import Image
from pathlib import Path
from torch.utils.data import Dataset


class SimpleClassificationDataset(Dataset):
    def __init__(self, csv_file, transform, phase=None):
        super(SimpleClassificationDataset, self).__init__()
        self.df = pd.read_csv(csv_file)
        self.transform = transform

        fields = ['img_path', 'label']
        for field in fields:
            if field not in self.df:
                raise ValueError

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        record = self.df.iloc[index]
        with open(record['img_path'], 'rb') as f:
            img = Image.open(f).convert('RGB')
        label = record['label']

        if self.transform:
            img = self.transform(img)

        return img, label


class SimpleClassificationFolder(Dataset):
    def __init__(self, root_dir, transform, phase=None):
        super(SimpleClassificationFolder, self).__init__()
        self.transform = transform

        root_dir = Path(root_dir)
        self.labels = [
            gt_dir.stem
            for gt_dir in root_dir.glob('*')
        ]

        records = []
        for label in self.labels:
            for p in (root_dir / label).glob('*.jpg'):
                records.append(dict(
                    img_path=str(p),
                    label=str(label)
                ))
        self.df = pd.DataFrame(records)

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        record = self.df.iloc[index]
        with open(record['img_path'], 'rb') as f:
            img = Image.open(f).convert('RGB')
        label = record['label']

        if self.transform:
            img = self.transform(img)

        return img, label, record['img_path']
