# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com

import os
import sys

sys.path.append(os.getcwd())

import torch
import deep.vneck.config as config
from shutil import copy
from torch import Tensor
from pathlib import Path
from torchvision import transforms
from torch.utils.data import DataLoader
from sklearn.metrics import classification_report
from deep.vneck.model import ResNet50
from deep.datasets.simple_classification import SimpleClassificationFolder

model_path = config.test_model
test_root = config.val_root
pred_dir = config.pred_dir

model, input_size = ResNet50(), 224
model.load_state_dict(torch.load(model_path))
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.cuda()
model.eval()

transforms = transforms.Compose([
    transforms.Resize(input_size),
    transforms.CenterCrop(input_size),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])
dataset = SimpleClassificationFolder(test_root, transforms)
dataloader = DataLoader(dataset, batch_size=32, num_workers=4)

true_list = []
pred_list = []
path_list = []

for inputs, labels, img_paths in dataloader:
    inputs = Tensor(inputs).to(device)

    outputs, features = model(inputs)
    preds = outputs.flatten() > 0.5

    true_list.extend([str(label) for label in labels])
    pred_list.extend([str(pred) for pred in preds.tolist()])
    path_list.extend(img_paths)

pred_dir = Path(pred_dir)
pred_dir.mkdir(exist_ok=True)
for label in dataset.labels:
    (Path(pred_dir) / label).mkdir(exist_ok=True)

cnt = 0
for label, pred, img_path in zip(true_list, pred_list, path_list):
    src_path = Path(img_path)
    dst_link = pred_dir / pred / f'{label}_{src_path.name}'
    # os.symlink(str(src_path), str(dst_link))
    copy(str(src_path), str(dst_link))
    if label == pred:
        cnt += 1

report = classification_report(true_list, pred_list, digits=4)
report += '\n\n'
report += f'acc: {cnt / len(pred_list):.4f}'

print(report)
(pred_dir / 'report.txt').write_text(report)
