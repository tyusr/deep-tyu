# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
import torch
import time
import copy
import torch.nn as nn
import pretrainedmodels
from torchvision.models import resnet50


class NASLarge(nn.Module):
    def __init__(self, num_classes=1, pretrained=None):
        super(NASLarge, self).__init__()
        self.backbone = pretrainedmodels.nasnetalarge(num_classes=num_classes)
        self.classifier = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(self.backbone.last_linear.out_features, num_classes)
        )
        self.sigmoid = nn.Sigmoid()

    def forward(self, *input):
        feature = self.backbone(*input)
        y = self.classifier(feature)
        y = self.sigmoid(y)
        return y, feature


class SEResNeXt50(nn.Module):
    def __init__(self, num_classes=1, pretrained=None):
        super(SEResNeXt50, self).__init__()
        self.backbone = pretrainedmodels.se_resnext50_32x4d()
        self.classifier = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(self.backbone.last_linear.out_features, num_classes)
        )
        self.sigmoid = nn.Sigmoid()

    def forward(self, *input):
        feature = self.backbone(*input)
        y = self.classifier(feature)
        y = self.sigmoid(y)
        return y, feature


class ResNet50(nn.Module):
    def __init__(self, num_classes=1, pretrained=None):
        super(ResNet50, self).__init__()
        self.backbone = pretrainedmodels.resnet50()
        self.classifier = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(self.backbone.last_linear.out_features, num_classes)
        )
        self.sigmoid = nn.Sigmoid()

    def forward(self, *input):
        feature = self.backbone(*input)
        y = self.classifier(feature)
        y = self.sigmoid(y)
        return y, feature


def get_model_nas(num_classes):
    model = pretrainedmodels.nasnetalarge(num_classes)


def get_model_resnet50(num_classes, use_pretrained=True):
    model = resnet50(use_pretrained)

    # for param in model.parameters():
    # param.requires_grad = False

    model.fc = nn.Linear(model.fc.in_features, num_classes)
    input_size = 224
    return model, input_size


def train_model(model, dataloaders, criterion, optimizer, scheduler, device, num_epochs, save_dir, writer):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    n_iter = 0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch + 1, num_epochs))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs, features = model(inputs)
                    # _, preds = torch.max(outputs, 1)
                    preds = outputs.flatten() > 0.5
                    loss = criterion(outputs.flatten(), labels.float())

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                        n_iter += 1

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.byte())

            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_acc = running_corrects.double() / len(dataloaders[phase].dataset)

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            if phase == 'train':
                writer.add_scalar('train_loss', epoch_loss, epoch)

            if phase == 'val':
                writer.add_scalar('val_acc', epoch_acc, epoch)

                # deep copy the model
                if epoch_acc + 0.1 > best_acc and epoch > 2:
                    best_acc = epoch_acc
                    best_model_wts = copy.deepcopy(model.state_dict())
                    torch.save(best_model_wts, f'{save_dir}/epoch_{epoch:03d}_acc_{epoch_acc:.4f}.pth')

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model
