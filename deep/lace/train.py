# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
import os
import sys

sys.path.append(os.getcwd())

import torch
import torch.nn as nn
import torch.optim as optim
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from deep.lace.model import train_model
# from deep.lace.model import get_model_resnet50 as get_model
from deep.lace.model import SEResNeXt50, ResNet50
import deep.lace.config as config
from torchvision import transforms, datasets

num_epochs = 200
batch_size = 64
use_pretrained = True
num_classes = 2

input_size = 224
model = ResNet50()
# model, input_size = get_model(num_classes, use_pretrained)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.cuda()

tb_dir = config.tensorboard_dir
model_dir = config.model_dir

train_root = config.train_root
val_root = config.val_root

data_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(input_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'val': transforms.Compose([
        transforms.Resize(input_size),
        transforms.CenterCrop(input_size),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}

# image_datasets = dict(
#     train=SimpleClassificationDataset(train_csv, data_transforms['train'], 'train'),
#     val=SimpleClassificationDataset(val_csv, data_transforms['val'], 'val'),
# )


image_datasets = dict(
    train=datasets.ImageFolder(train_root, data_transforms['train']),
    val=datasets.ImageFolder(val_root, data_transforms['val']),
)

dataloaders_dict = {
    x: DataLoader(image_datasets[x], batch_size=batch_size, shuffle=True, num_workers=4)
    for x in ['train', 'val']
}

optimizer = optim.Adam(model.parameters(), lr=0.001)

# weights = [1.0, 2.0]
# criterion = nn.CrossEntropyLoss(weight=torch.FloatTensor(weights).cuda())
weights = torch.FloatTensor([2.0]).cuda()
criterion = nn.BCELoss(weight=weights)

exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=4, gamma=0.5)

writer = SummaryWriter(log_dir=tb_dir)
model = train_model(model,
                    dataloaders_dict,
                    criterion,
                    optimizer,
                    exp_lr_scheduler,
                    device,
                    num_epochs,
                    model_dir,
                    writer)
