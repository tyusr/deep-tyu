# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
import os
import sys

sys.path.append(os.getcwd())

import pandas as pd
from pathlib import Path
from random import shuffle

if __name__ == '__main__':
    pos_dir = Path('./data/lace/lace-pos')
    neg_dir = Path('./data/lace/lace-neg')
    src_root_dir = Path('/data/open_data/DeepFashion/Img/img_highres/')
    dst_root_dir = Path('./data/lace/')

    file_list = dict(
        pos=list(pos_dir.glob('*.jpg')),
        neg=list(neg_dir.glob('*.jpg'))
    )

    records = []
    for part, f_list in file_list.items():
        for p in f_list:
            _ = p.name.split('img_')
            img_name = 'img_' + _[-1]
            dir_name = _[-2][:-1]
            records.append(dict(
                img_path=str(src_root_dir / dir_name.title() / img_name),
                label=1 if part == 'pos' else 0
            ))

    df = pd.DataFrame(records)

    split_train = 0.8
    split_val = 1.0

    train_set, val_set, test_set = [], [], []

    subsets = []
    for label, sub in df.groupby('label'):
        n = len(sub)
        indices = list(range(n))
        shuffle(indices)

        sep_train = int(n * split_train)
        sep_val = int(n * split_val)

        subsets.append((sub.iloc[:sep_train], sub.iloc[sep_train:sep_val], sub.iloc[sep_val:]))

    train_set, val_set, test_set = zip(*subsets)

    subset_dict = dict(
        train=pd.concat(train_set).drop_duplicates(),
        val=pd.concat(val_set).drop_duplicates(),
        test=pd.concat(test_set).drop_duplicates()
    )

    for phash, df in subset_dict.items():
        dst_phash = dst_root_dir / str(phash)
        dst_phash.mkdir(exist_ok=True)
        for label, sub in df.groupby('label'):
            dst_label = dst_phash / str(label)
            dst_label.mkdir(exist_ok=True)
            for idx, r in sub.iterrows():
                p = Path(r['img_path'])
                os.symlink(str(p), str(dst_label / f'{p.parent.name}_{p.name}'))

    # for k, v in subset_dict.items():
    #     if len(v) > 0:
    #         v.to_csv(f'./data/lace/{k}.csv', index=False)
