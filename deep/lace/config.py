train_name = '0814-lace-resnet_50'
test_name = '0814-lace-resnet_50'
test_model_name = 'epoch_007_acc_0.8503.pth'

# ============================================================

root_dir = '/data/tyu/deep-tyu/'

train_root = f'{root_dir}/data/lace/train/'
val_root = f'{root_dir}/data/lace/val/'

tensorboard_dir = f'{root_dir}/logs/{train_name}/'
model_dir = f'{root_dir}/logs/{train_name}/'

pred_dir = f'{root_dir}/logs/{test_name}/pred/'
test_model = f'{root_dir}/logs/{test_name}/{test_model_name}'
